import Api from './api';

const SpentService ={

    create: (newSpentModel) => Api.post('spents/', newSpentModel),
    show: (spentId) => Api.get(`spents/${spentId}`),
    list: (pageableModel) => Api.get(`spents${pageableModel.buildQueryString()}`)

}

export default SpentService;