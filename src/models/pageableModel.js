import SortModel from './sortModel';

export default class PageableModel{

    constructor(page = 0, size = 20, sort = new SortModel()){
        this.page = page;
        this.size = size;
        this.sort = sort;
    }

    buildQueryString = () => {
        let queryString = `?size=${this.size}&page=${this.page}`
        if (this.sort){
            queryString += `&sort=${this.sort.fieldName},${this.sort.fieldSort}`;
        }
        return queryString;
    }

}