export default class Sort{

    constructor(fieldName = 'id', fieldSort = "asc"){
        this.fieldName = fieldName;
        this.fieldSort = fieldSort;
    }

}