import moment from 'moment';

export default class SpentListModel{

    constructor(data){
        if (data){
            this.id = data.id;
            this.owner = data.owner;
            this.description = data.description;
            this.price = data.price;
            this.dateTime = data.dateTime;
            this.tags = data.tags;
        }
    }


    get formatedPrice(){
        return (this.price) ? 
            this.price.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }) :
            0.00;
    }

    get formatedDateTime(){
        return (this.dateTime) ? 
            moment(this.dateTime).format("DD/MM/YYY HH:mm:ss") :
            "";
    }

}