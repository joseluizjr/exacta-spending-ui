import SpentListModel from './spentListModel';

class PageableModel{

    constructor(data){
        this.sort = new SortModel(data.sort);
        this.offset = data.offset;
        this.pageNumber = data.pageNumber;
        this.pageSize = data.pageSize;
        this.paged = data.paged;
        this.unpaged = data.unpaged;
    }
}

class SortModel{

    constructor(data){
        this.sorted = data.sorted
        this.unsorted = data.unsorted
        this.empty = data.empty;
    }

}

export default class PageSpentModel{

    constructor(data){
        if (data){
            this.content = data.content.map(s => new SpentListModel(s));
            this.pageable = new PageableModel(data.pageable);
            this.totalPages = data.totalPages;
            this.totalElements = data.totalElements;
            this.last = data.last;
            this.number = data.number;
            this.sort = new SortModel(data.sort);
            this.size = data.size;
            this.first = data.first;
            this.numberOfElements = data.numberOfElements
            this.empty = data.empty;
        }
    }

}