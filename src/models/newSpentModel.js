export default class NewSpentModel{

    constructor(owner, description, dateTime, price, tags){
        this.owner = owner;
        this.description = description;
        this.dateTime = dateTime;
        this.price = price;
        this.tags = tags.join();
    }

}