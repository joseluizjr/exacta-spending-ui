import React, {Fragment} from 'react';
import Header from '../../../components/shared/header';
import Footer from '../../../components/shared/footer';
import ListSpentForm from '../../../components/spents/listSpentForm';

const SpentList = () =>{
    return (
        <Fragment>
            <Header title="Lista de Gastos"/>
            <ListSpentForm />
            <Footer />
        </Fragment>
    )
};

export default SpentList;