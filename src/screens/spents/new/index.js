import React, {Fragment} from 'react';
import Header from '../../../components/shared/header';
import Footer from '../../../components/shared/footer';
import NewSpentForm from '../../../components/spents/newSpentForm';

const NewSpent = () => {
    return(
        <Fragment>
            <Header title="Novo Gasto"/>
            <NewSpentForm />
            <Footer />
        </Fragment>
    )
};

export default NewSpent;