import React, {Fragment} from 'react';
import Header from '../../../components/shared/header';
import Footer from '../../../components/shared/footer';
import DetailSpentForm from '../../../components/spents/detailSpentForm';



const SelectedSpent = () =>{ 

    return(
        <Fragment>
            <Header title="Detalhes do Gasto"/>
            <DetailSpentForm />
            <Footer />
        </Fragment>
    )

};
export default SelectedSpent;