import React, { Fragment } from 'react';
import '../../../styles/footer.scss'

const Footer = (props) => (
    <Fragment>
        <div className="divFooter">
            <label className="copyright">
            © 2020 ExactaWorks. Todos os direitos reservados
            </label>
        </div>
    </Fragment>
)

export default Footer