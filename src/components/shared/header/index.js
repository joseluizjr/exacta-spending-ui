import React, { Fragment } from 'react';
import '../../../styles/header.scss'

const Header = (props) => (
    <Fragment>
        <div className="divHeader" >
            <h1>Exacta Spending, seu controle inteligente</h1>
        </div>
        <h2 className="subtitle">{props.title}</h2>
    </Fragment>
)

export default Header