import React, { useState, Fragment, useEffect } from 'react';
import { Redirect, useParams } from 'react-router-dom';
import SpentService from '../../../services/spentService';
import SpentDetailModel from '../../../models/spentDetailModel';
import '../../../styles/detailSpentForm.scss';
import { Card } from 'primereact/card';
import { Button } from 'primereact/button';
import {ProgressSpinner} from 'primereact/progressspinner';
import { Chips } from 'primereact/chips';

const DetailSpentForm = () =>{

    let { spentId } = useParams();

    const routesOption ={
        LIST: '/spents',
        NEW: '/spents/new',
        CURRENT: ''
    }

    const [selectedSpent, setSelectedSpent] = useState(new SpentDetailModel(undefined))
    const [redirectToAnother, setRedirectToAnother] = useState(routesOption.CURRENT)


    const findSpent = async () => { 
        try{
            const response = await SpentService.show(spentId);
            setSelectedSpent(new SpentDetailModel(response.data));
        }catch(error){
        }
    }

    useEffect(() => {
        findSpent(); 
    }, []);// eslint-disable-line react-hooks/exhaustive-deps

    if (redirectToAnother !== routesOption.CURRENT){
        return <Redirect to={{pathname: redirectToAnother}} />
    }

    const footer = <span>
        <div className="p-grid">
            <div className="p-col-3"></div>
            <div className="p-col-2">
                <Button onClick={() => setRedirectToAnother(routesOption.LIST)} 
                    label="Lista de gastos" className="p-button-primary p-button-rounded"/>
            </div>
            <div className="p-col-2"></div>
            <div className="p-col-2">
                <Button onClick={() => setRedirectToAnother(routesOption.NEW)} 
                    label="Registrar novo gasto" className="p-button-primary p-button-rounded"/>
            </div>
            <div className="p-col-3"></div>
        </div>
    </span>

    return(
        <Fragment>
            <Card footer={footer} className="SpentDetailCard">
                { selectedSpent.id ?
                    <Fragment>
                        <div className="p-grid p-fluid">
                            <div className="p-col-12">
                                <h3>Nome do usuário: </h3><label>{selectedSpent.owner}</label>
                                {redirectToAnother}
                            </div>
                        </div>
                        <div className="p-grid p-fluid">
                            <div className="p-col-12">
                                <h3>Descrição: </h3><label>{selectedSpent.description}</label>
                            </div>
                        </div>
                        <div className="p-grid p-fluid">
                            <div className="p-col-6">
                                <h3>Data e Hora: </h3><label>{selectedSpent.formatedDateTime}</label>
                            </div>
                            <div className="p-col-6">
                                <h3>Valor: </h3><label>{selectedSpent.formatedPrice}</label>
                            </div>
                        </div>
                        <div className="p-grid p-fluid">
                            <div className="p-col-12">
                                <h3>Tags: </h3>
                                <Chips className="readOnlyChips" 
                                    value={selectedSpent.tags.split(',')}
                                    ></Chips>
                            </div>
                        </div>
                    </Fragment>
                    :
                    <div className="p-grid">
                        <div className="p-col-5"></div>
                        <div className="p-col-7">
                            <ProgressSpinner/>
                        </div>
                    </div>
                }
            </Card>
        </Fragment>
    )

}

export default DetailSpentForm;