import React, { useState, Fragment, useRef } from 'react';
import { Card } from 'primereact/card';
import { Button } from 'primereact/button';
import { InputText } from 'primereact/inputtext';
import { Calendar } from 'primereact/calendar';
import { InputNumber } from 'primereact/inputnumber';
import { Chips } from 'primereact/chips';
import NewSpentModel from '../../../models/newSpentModel';
import SpentDetailModel from '../../../models/spentDetailModel';
import SpentService from '../../../services/spentService';
import moment from 'moment';
import { Redirect } from 'react-router-dom';
import '../../../styles/newSpentForm.scss';
import {Growl} from 'primereact/growl';
import {Dialog} from 'primereact/dialog';

const NewSpentForm = () =>{

    let growl = useRef(null);
    const [owner, setOwner] = useState('');
    const [description, setDescription] = useState('');
    const [dateTime, setDateTime] = useState('');
    const [price, setPrice] = useState(0);
    const [tags, setTags] = useState([]);
    const [redirectTo, setRedirectTo] = useState(undefined);
    const [showDialog, setShowDialog] = useState(false);

    const pt = {
        firstDayOfWeek: 1,
        dayNames: ["domingo", "segunda-feira", "terça-feira", "quarta-feira", "quinta-feira", "sexta-feira", "sábado"],
        dayNamesShort: ["dom", "seg", "ter", "qua", "qui", "sex", "sáb"],
        dayNamesMin: ["D", "S", "T", "QA", "QI", "SE", "SA"],
        monthNames: ["janeiro", "feverairo", "março", "abril", "maio", "junho", "julho", "agosto", "setembro", "outubro", "novembro", "dezembro"],
        monthNamesShort: ["jan", "fev", "mar", "abr", "mai", "jun", "jul", "ago", "set", "out", "nov", "dez"],
        today: 'Hoje',
        clear: 'Limpar',
        dateFormat: 'dd/mm/yy',
        weekHeader: 'Sm'
    };

    const NewSpent = async (e) =>{
        try{
            const response = await SpentService
                .create(new NewSpentModel(owner, description, dateTime, price, tags));
            setRedirectTo(`/spents/${new SpentDetailModel(response.data).id}`);
        }catch(error){
            growl.current.show({
                severity: 'error', 
                summary: 'Erro', 
                detail: 'Erro ao tentar criar novo gasto.'
            });
        }
    }

    const EnableSave = () => {
        return !(owner.length > 2 && description.length > 2 && 
                moment(dateTime).isValid() && price > 0.00 && tags.length > 0)
    }

    const cardFooter = <span className="cardFooter">
        <div className="p-grid">
            <div className="p-col-3"></div>
            <div className="p-col-2">
                <Button onClick={() => setShowDialog(true)} disabled={EnableSave()} label="Salvar" icon="pi pi-save" className="p-button-primary p-button-rounded"/>
            </div>
            <div className="p-col-2"></div>
            <div className="p-col-2">
                <Button onClick={() => setRedirectTo("/spents")} 
                    label="Voltar Para Lista" className="p-button-primary p-button-rounded"/>
            </div>
            <div className="p-col-3"></div>
        </div>
    </span>

    if (redirectTo){
        return <Redirect to={{pathname: redirectTo}} />
    }

    const dialogFooter = () => {
        return (
            <div>
                <Button label="Sim" icon="pi pi-check" onClick={() => NewSpent()}/>
                <Button label="Não" icon="pi pi-times" 
                    onClick={() => setShowDialog(false)} className="p-button-secondary"/>
            </div>
        );
    }

    return(
        <Fragment>
            <Growl ref={growl} />
            <Dialog header="Novo Gasto" visible={showDialog} 
                footer={dialogFooter()} onHide={() => dialogFooter()} >
                <p>Deseja realmente criar um novo gasto?</p>
            </Dialog>
            <Card footer={cardFooter} className="newSpentCard">
                <div className="p-grid lineInputs">
                    <div className="p-col-4 p-fluid">
                        <span className="p-float-label">
                            <InputText type="text" 
                                value={owner} onChange={(e) => setOwner(e.target.value)}
                                minLength={2} maxLength={150}/>
                            <label>Usuário</label>
                        </span>
                    </div>
                    <div className="p-col-8 p-fluid">
                        <span className="p-float-label">
                            <InputText type="text" 
                                value={description} onChange={(e) => setDescription(e.target.value)}
                                minLength={2} maxLength={150}/>
                            <label>Descrição</label>
                        </span>
                    </div>
                </div>

                <div className="p-grid lineInputs">
                    <div className="p-col-2 p-fluid">
                        <label htmlFor="dateTime">Data e hora</label>
                        <Calendar locale={pt} value={dateTime} 
                        onChange={(e) => setDateTime(e.value)} showTime={true} 
                        showSeconds={true} id="dateTime"/>
                    </div>
                    <div className="p-col-2 p-fluid">
                        <label htmlFor="price">Valor</label>
                        <InputNumber prefix="R$" value={price} onChange={(e) => setPrice(e.value)} 
                            mode="decimal" locale="pt-BR" maxFractionDigits={2} 
                            minFractionDigits={2} max={10000000000000.00} min={0.00} />
                    </div>
                    <div className="p-col-8 p-fluid">
                        <label>Tags (digite ',' para separar as tags)</label>
                        <Chips className="inputChips" value={tags} 
                        onChange={(e) => setTags(e.value)} separator=','></Chips>
                    </div>
                </div>
            </Card>
        </Fragment>
    )

}

export default NewSpentForm