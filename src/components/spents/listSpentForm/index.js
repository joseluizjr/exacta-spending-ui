import React, { useState, Fragment, useEffect } from 'react';
import { DataTable } from 'primereact/datatable';
import {Column} from 'primereact/column';
import { Card } from 'primereact/card';
import { Button } from 'primereact/button';
import SpentService from '../../../services/spentService';
import PageSpentModel from '../../../models/pageSpentModel';
import { Redirect } from 'react-router-dom';
import '../../../styles/listSpentForm.scss';
import PageableModel from '../../../models/pageableModel';
import SortModel from '../../../models/sortModel';
import {ProgressSpinner} from 'primereact/progressspinner';

const ListSpentForm = () =>{

    const [spentPageList, setSpentPageList] = useState(new PageSpentModel(undefined));
    const [loading, setLoading] = useState(true);
    const [sortModel, SetSortModel] = useState(new SortModel());
    const [pageableModel, setPageableModel] = useState(new PageableModel());
    const [sortField, setSortField] = useState({field: 'id', order: 1});
    const [redirectTo, setRedirectTo] = useState(undefined)

    useEffect(() => {
        findSpents();
    },[]);// eslint-disable-line react-hooks/exhaustive-deps

    const findSpents = async () => {
        try{
            console.log(window.env)
            const response = await SpentService.list(pageableModel);
            setSpentPageList(s => new PageSpentModel(response.data));
        }catch(error){

        }finally{
            setLoading(false);
        }
    }

    const onPage = (event) =>{
        setLoading(true);
        setPageableModel(new PageableModel(event.page,event.rows, sortModel));
        findSpents();
    }

    const onSort = (event) =>{
        setLoading(true);
        setSortField({field: event.sortField, order: event.sortOrder});
        SetSortModel(new SortModel(sortField.field, sortField.order === 1? "asc": "desc"));
        setPageableModel(new PageableModel(spentPageList.pageable.pageNumber, 
            spentPageList.pageable.pageSize, sortModel));
        findSpents();
    }

    const getFormatedPrice  = (data, column) => 
        <span className="centerColumnContent">{data.formatedPrice}</span>
    const getFormatedDateTime = (data, column) => 
        <span className="centerColumnContent">{data.formatedDateTime}</span>
    const formatColumn = (data, column, columnName) =>
        <span className="centerColumnContent">{data[columnName]}</span>
    const formatOptionsColumn = (data, column) =>
        <span className="centerColumnContent">
            <Button onClick={() => setRedirectTo("/spents/" + data.id)} 
                    label="Detalhes" className="p-button-primary p-button-rounded"/>
        </span>

    const columns = [
        { field: 'id', header: 'Código', body: (data, column) => formatColumn(data, column, 'id'), sortable: true},
        { field: 'owner', header: 'Nome do usuário', body: (data, column) => formatColumn(data, column, 'owner'), sortable: true},
        { field: 'description', header: 'Descrição', body: (data, column) => formatColumn(data, column, 'description'), sortable: true},
        { field: 'price', header: 'Valor', body : getFormatedPrice , sortable: true},
        { field: 'dateTime', header: 'Date e Hora do registro', body: getFormatedDateTime, sortable: true},
        { field: 'options', header: 'Opções', body: formatOptionsColumn , sortable: false}
    ];

    const dynamicColumns = columns.map((c, i) =>
        <Column key={c.field} field={c.field} header={c.header} body={c.body} sortable={c.sortable}/>
    )

    if (redirectTo){
        return <Redirect to={{pathname: redirectTo}} />
    }

    const footer = <span className="cardFooter">
            <div className="p-grid">
                <div className="p-col-5"></div>
                <div className="p-col-2">
                    <Button onClick={() => setRedirectTo('/spents/new')} label="Novo Gasto" 
                        className="p-button-primary p-button-rounded"/>
                </div>
                <div className="p-col-5"></div>
            </div>
        </span>

    return(
        <Fragment>
            <Card footer={footer} className="listSpentCard">
                { spentPageList.content ?
                    <DataTable value={spentPageList.content} paginator={true} 
                        lazy={true} onPage={onPage} loading={loading}
                        rows={spentPageList.pageable.pageSize} 
                        first={spentPageList.pageable.offset}
                        totalRecords={spentPageList.totalElements} 
                        rowsPerPageOptions={[10,20,30]}
                        onSort={onSort}
                        sortField={sortField.field} sortOrder={sortField.order}
                        removableSort={false}>
                        {dynamicColumns}
                    </DataTable> 
                    : 
                    <div className="p-grid">
                        <div className="p-col-5"></div>
                        <div className="p-col-7">
                            <ProgressSpinner/>
                        </div>
                    </div>
                }
            </Card>
        </Fragment>
    )

}

export default ListSpentForm;