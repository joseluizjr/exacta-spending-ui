import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import SpentList from './screens/spents/list';
import NewSpent from './screens/spents/new';
import SelectedSpent from './screens/spents/show';

const Routes = () =>(
    <BrowserRouter>
        <Switch>
            <Route exact path="/spents" component={SpentList} />
            <Route path="/spents/new" component={NewSpent} />
            <Route path="/spents/:spentId" component={SelectedSpent} />
        </Switch>
    </BrowserRouter>
)

export default Routes;