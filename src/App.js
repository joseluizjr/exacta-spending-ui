import React, { Fragment } from 'react';
import './App.scss';
import Routes from './routes';
import 'primereact/resources/themes/nova-light/theme.css'
import 'primereact/resources/primereact.min.css'
import 'primeicons/primeicons.css'
import 'primeflex/primeflex.css'

const App = () =>(
	<Fragment>
		<Routes />
	</Fragment>
)


export default App;
